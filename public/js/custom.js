function escogerfondo(numero) {
  switch (numero) {
    case 1:
      document.body.style.backgroundImage = "url('./img/fondo1.png')";
      break;
    case 2:
      document.body.style.backgroundImage = "url('./img/fondo2.jpg')";
      break;
    case 3:
      document.body.style.backgroundImage = "url('./img/fondo3.jpg')";
      break;
    case 4:
      document.body.style.backgroundImage = "url('./img/fondo4.jpg')";
      break;
    default:
    document.body.style.background = "lightgrey";
  }
}
function reload(){
    colornavbar = document.getElementById("colornavbar");
    document.getElementById("navbarchangecolor").style.background = colornavbar.value;
    colornavbar = document.getElementById("colorfooter");
    document.getElementById("pie").style.background = colornavbar.value;
    titleh1 = document.getElementById("titleh1").value;
    document.getElementById("titplaning").innerHTML  = titleh1;

}
function cambiocolornavbar(){
    colornavbar = document.getElementById("colornavbar");
    document.getElementById("navbarchangecolor").style.background = colornavbar.value;
}
function cambiocolorpie(){
    colornavbar = document.getElementById("colorfooter");
    document.getElementById("pie").style.background = colornavbar.value;
}
function cambioh1(){
    titleh1 = document.getElementById("titleh1").value;
    document.getElementById("titplaning").innerHTML  = titleh1;

}

function cambiofuente() {
    var selectorfuente = document.getElementById('selectorfuente');
    var fuente = selectorfuente.options[selectorfuente.selectedIndex].value;
    var titplaning = document.getElementById('titplaning')
    titplaning.style.fontFamily = fuente;
    selectorfuente.style.fontFamily = fuente;
}

