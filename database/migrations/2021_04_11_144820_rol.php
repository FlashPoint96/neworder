<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rol', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('TipusUsuari');
            $table->string('DescripcioRol');
            $table->enum('EnumRol', ['UsuariNormal', 'UsuariAdministradorComunitat', 'UsuariAdministradorNewOrder']);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Rol');
    }
}
