<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Objetos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Objetos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('NomObjeto');
            $table->string('DescripcioObjeto');
            $table->Integer('PreuObjeto');
            $table->enum('Categoria', ['Armas', 'Chaleco', 'DF', 'Navajas','Etc']);	
            $table->string('Path', 235);
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('Users');
        });

        
    Schema::table('users', function (Blueprint $table) {
        $table->BigInteger('rol_id')->nullable()->unsigned();
        $table->foreign('rol_id')->references('id')->on('Rol');
        $table->BigInteger('comunitat_id')->nullable()->unsigned();
        $table->foreign('comunitat_id')->references('id')->on('Comunitat');
    });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Objetos');
    }
}
