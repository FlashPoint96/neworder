<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Comunitat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Comunitat', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('NomComunitat');
            $table->string('DescripcioComunitat');
            $table->boolean('Activa')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Comunitat');
    }
}
