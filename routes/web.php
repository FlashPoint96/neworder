<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controlador;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[Controlador::class,'LogeadoNo']);

Route::post('/registeroLogin',[Controlador::class,'RegisterLogin']);
Route::get('/pagina2',function(){
	return view("".'pantalla2'); 
});
Route::get('/pagina3',function(){
	return view("".'pantalla3');
});
Route::get('/pagina4',[Controlador::class,'pagina4']);

Route::get('/objeto',function(){
	return view("".'newItem');
});
Route::get('/index',function(){
	return view("".'dashboard');
});

Route::get('/pagina2',[Controlador::class,'pagina2']);
Route::get('/Controlador/buyItem/{tbid}',[Controlador::class,'comprarObjeto']);
Route::get('/Controlador/trabajar/{tbid}',[Controlador::class,'trabajarDinero']);

Route::post('/objetos',[Controlador::class,'agregarItem']);
Route::get('/pantalla5',[Controlador::class,'pagina5'])->name('pagina.Admin');;
Route::get('/logOut',[Controlador::class,'logout']);
Route::get('/matar',[Controlador::class,'MatarCiudad'])->name('Matar');;

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

