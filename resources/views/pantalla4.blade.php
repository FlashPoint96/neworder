@section('content')
<!DOCTYPE html>
<html lang="es">
<head>
    <script src="{{ mix('js/app.js') }}"></script>
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
</head>
<body>
    <nav id="navbarchangecolor" class="navbar navbar-expand-lg navbar-light page__menu page__custom-settings menu">
        <ul class="menu__list r-list">
            <div class="logo">
                <p>N|E</p>
            </div>
            <li class="menu__group"><a href="{{ asset('/') }}" class="menu__link r-link text-underlined">Inici </a></li>
            <li class="menu__group"><a href="{{ asset('/pagina2') }}" class="menu__link r-link text-underlined">Tienda </a></li>
            <li class="menu__group"><a href="{{ asset('/pagina4') }}" class="menu__link r-link text-underlined">Parametrizable</a></li>            
        </ul>
    </nav>

  <div class="container">
    <div id="muro">
      <h1 id="titplaning">Planning the next ploy</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <input class="form-control form-control-lg" id="titcoment" type="text" placeholder="Título del comentario">
        <textarea  class="form-control form-control-lg" id="contencomen" type="" placeholder="Contenido del comentario" rows="5"></textarea>
        <div class="btn btn-dark" onclick="publicar();">Publicar</div>
      </div>
    </div>
  </div>
  <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
  <!-- <div> -->
    <div class="offcanvas-header">
      <h5 id="offcanvasRightLabel">Configuration</h5>
      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">

      <div class="row">
        <div class="col-md-6" id="bg1" onclick="escogerfondo(1);">

        </div>
        <div class="col-md-6" id="bg2" onclick="escogerfondo(2);">

        </div>
      </div><div class="row">
        <div class="col-md-6" id="bg3" onclick="escogerfondo(3);">

        </div>
        <div class="col-md-6" id="bg4" onclick="escogerfondo(4);">

        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <label for="colornavbar" class="form-label">Color navbar</label>
          <input type="color" class="form-control form-control-color" id="colornavbar" onchange="cambiocolornavbar();" value="#5f5f5f" title="Escoge tu color">
        </div>
        <div class="col-md-6">
          <label for="colorfooter" class="form-label">Color footer</label>
          <input type="color" class="form-control form-control-color" id="colorfooter" onchange="cambiocolorpie();" value="#5f5f5f" title="Escoge tu color">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <input class="form-control form-control-lg" id="titleh1" onchange="cambioh1();" type="text" placeholder="Title the next ploy">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <select class="form-select form-select-lg mb-3 styled-select" aria-label=".form-select-lg example" id="selectorfuente" onchange="cambiofuente();">
            <option value="Rock Salt" selected style="font-family: Rock Salt;">Rock Salt</option>
            <option style="font-family: Source Sans Pro;">Source Sans Pro</option>
            <option style="font-family: Cuprum;">Cuprum</option>
            <option style="font-family: Dosis;">Dosis</option>
            <option style="font-family: PT Sans;">PT Sans</option>
            <option style="font-family: Armata;">Armata</option>
            <option style="font-family: Ropa Sans;">Ropa Sans</option>
            <option style="font-family: Istok Web;">Istok Web</option>
            <option style="font-family: Arimo;">Arimo</option>
            <option style="font-family: Ubuntu;">Ubuntu</option>
            <option style="font-family: Ubuntu Condensed;">Ubuntu Condensed</option>
            <option style="font-family: Pathway Gothic One;">Pathway Gothic One</option>
            <option style="font-family: Roboto;">Roboto</option>
            <option style="font-family: Scada;">Scada</option>
            <option style="font-family: Quattrocento Sans;">Quattrocento Sans</option>
            <option style="font-family: Courgette;">Courgette</option>
            <option style="font-family: Radley;">Radley</option>
            <option style="font-family: Archivo Narrow;">Archivo Narrow</option>
            <option style="font-family: Oswald;">Oswald</option>
            <option style="font-family: Open Sans;">Open Sans</option>
            <option style="font-family: Inconsolata;">Inconsolata</option>
            <option style="font-family: Karla;">Karla</option>
            <option style="font-family: Molengo;">Molengo</option>
            <option style="font-family: Rosario;">Rosario</option>
            <option style="font-family: Roboto Condensed;">Roboto Condensed</option>
            <option style="font-family: Play;">Play</option>
            </select>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <i class="fas fa-sync icon" onclick="reload();"></i>
        </div>
      </div>
    </div>
  </div>





  <footer id="pie">
    <button class="btn btn-dark btnmenu" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"><i class="fas fa-microchip"></i></button>
    <ul id="webticker" >
            <li data-update="item1">barrio Matalascañá: esta madrugada se ha producido una explosion bioQuimica en las inmediaciones del centro de recursos organicos del barrio Matalascañá. de las 10 victimas del atentado solo una ha sido de caracter mortal pero toda la cosecha de esta temporada se ha visto afectada...
.</li>
            <li data-update="item2">Distrito Kingujo: las victimas del maremoto del distrito Kingujo siguen en aumento. 1438 victimas mortales, 4873 victimas graves y 7392 leves...
</li>
</ul>
    <h2>&copy;Copyright per Shahraz,Toni i Rafa<h2>

  </footer>
  <script src="https://kit.fontawesome.com/57906bfb21.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  <script src="./js/custom.js"></script>
  <script type="text/javascript">

  function publicar() {
    var user = {!! json_encode($Objecte) !!};
    var titcome = document.getElementById('titcoment');
    titcome = document.createTextNode("  :"+titcome.value+"\n");
    var contcomen =  document.getElementById('contencomen');
    contcomen = document.createTextNode(contcomen.value);
    var author = document.createTextNode(user);
    var newDiv = document.createElement("div");
    newDiv.appendChild(author);
    newDiv.appendChild(titcome);
    newDiv.appendChild(contcomen);
    newDiv.setAttribute("class", "contenedorcomentario");
    var parent = document.getElementById('titplaning');
    parent.appendChild(newDiv);
    document.getElementById('muro').insertBefore(newDiv, parent);
}

    AOS.init();
$("#webticker").webTicker({
    height:'75px',
    duplicate:true,
    rssfrequency:0,
    startEmpty:false,
    hoverpause:false,
    transition: "ease"
});

</script>
</body>
</html>
