<!DOCTYPE html>
<html lang="es">
<head>
<link href="{{ asset('/css/dashboard.css') }}" rel="stylesheet">
<script src="{{ mix('js/app.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.css'>
<link rel="stylesheet" href="{{ asset('/css/styleBootstrap.css') }}">
<link rel='stylesheet' href='https://cdn.oesmith.co.uk/morris-0.5.1.css'>
</head>
<body class="bodyDashboard">        

<div class="thunder" id="BodyEnteroCosas">
  <div id="loading"></div>
      <canvas id="canvas1"></canvas>
      <canvas id="canvas2"></canvas>
      <canvas id="canvas3"></canvas>
      
    <svg id="hs" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
    <defs>
        
        <filter id="glow">
              <fegaussianblur class="blur" result="coloredBlur" stddeviation="8"></fegaussianblur>
              <femerge>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="SourceGraphic"></femergenode>
              </femerge>
      </filter>
      
      <filter id="filter-broken" filterUnits="objectBoundingBox" x="0" y="0" width="100%" height="100%" color-interpolation-filters="sRGB">
          <feImage preserveAspectRatio="xMidYMid meet" xlink:href="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 800 800'%3E%3Crect width='70%25' height='70%25' fill='white'/%3E%3Ccircle cx='50%25' cy='50%25' r='500' %0Afill='none' stroke='black' stroke-width='950' stroke-dasharray='200'/%3E%3C/svg%3E" result="lense"/>
          <feDisplacementMap scale="10" xChannelSelector="B" yChannelSelector="G" in2="lense" in="SourceGraphic" result="glass-effect"/>
          <feMerge>
            <feMergeNode in="SourceGraphic"/>
            <feMergeNode in="glass-effect"/>
          </feMerge>
      </filter>
    </defs>
      
        <style type="text/css">
      <![CDATA[
        #svg_8{transform-origin:center;}
          
    #hs {
      position: absolute; 
      top: 50%; 
      left: 50%; 
      transform: translate(-50%, -50%); 
      filter: url('#filter-broken');
      /*
      url('#glow');
      */
    }
    #svg_12 {
      url('#glow');
      mix-blend-mode:multiply;
      transform-origin:center;
    }
    .path {
      animation:  animation1 3s ease-in-out 1s infinite alternate;
      fill-opacity: .25;
    }
    .p_0{
      animation:  animation2 3s ease-in-out .5s infinite alternate;
    }

    @keyframes animation1 {
      from {transform: perspective(400px) rotateX(20deg);}
      to {transform: perspective(400px) rotateX(-20deg);}
      from {transform: perspective(400px) rotateY(20deg);}
      to {transform: perspective(400px) rotateY(-20deg);}  
      from {stroke-width: 1;}
      to {stroke-width: 9;}
    }

    @keyframes animation2 {
      from {transform: perspective(400px) rotateX(20deg);}
      to {transform: perspective(400px) rotateX(-20deg);}
      from {transform: perspective(400px) rotateY(20deg);}
      to {transform: perspective(400px) rotateY(-20deg);}  
      from {stroke-width: 1;}
      to {stroke-width: 2;}
      from {stroke-opacity: .4;}
      to {stroke-opacity: 1;}
    }

      ]]>
      </style>
      
    
      
    <g>   
    <g fill="#1a1b1f" id="svg_8">
      <path class="p_0" stroke-width="1.5" stroke="#000000" id="svg_6" d="m302.47663,282.55895c-11.5319,-4.19342 -12.58025,5.76595 -33.28525,-1.57253l-40.62373,-14.67696l40.62373,-14.67696c20.705,-7.60057 21.75335,2.62089 33.28525,-1.57253c4.71759,-1.57253 7.60057,-7.86266 5.76595,-12.58025c-1.57253,-4.71759 -2.88297,-4.19342 -4.45551,-8.91101c-1.57253,-4.71759 -0.52418,-4.97968 -2.09671,-9.69728c-1.57253,-4.71759 -7.86266,-7.60057 -12.58025,-5.76595c-11.5319,4.19342 -5.76595,12.58025 -26.47095,20.18082l-62.63917,22.27753l-62.63917,-22.80171c-20.705,-7.60057 -15.20114,-15.9874 -26.47095,-20.18082c-4.71759,-1.57253 -11.00772,1.31044 -12.58025,5.76595c-1.57253,4.71759 -0.52418,4.97968 -2.09671,9.69728c-1.57253,4.71759 -2.88297,4.19342 -4.45551,8.91101c-1.57253,4.71759 1.31044,11.00772 5.76595,12.58025c11.5319,4.19342 12.58025,-5.76595 33.28525,1.57253l40.62373,14.67696l-40.62373,14.67696c-20.705,7.60057 -21.75335,-2.62089 -33.28525,1.57253c-4.71759,1.57253 -7.60057,7.86266 -5.76595,12.58025c1.57253,4.71759 2.88297,4.19342 4.45551,8.91101c1.57253,4.71759 0.52418,4.97968 2.09671,9.69728c1.57253,4.71759 7.86266,7.60057 12.58025,5.76595c11.5319,-4.19342 5.76595,-12.58025 26.47095,-20.18082l62.63917,-22.27753l62.63917,22.80171c20.705,7.60057 15.20114,15.9874 26.47095,20.18082c4.71759,1.57253 11.00772,-1.31044 12.58025,-5.76595c1.57253,-4.71759 0.52418,-4.97968 2.09671,-9.69728c1.57253,-4.71759 2.88297,-4.19342 4.45551,-8.91101c1.83462,-4.71759 -1.04835,-11.00772 -5.76595,-12.58025z" fill="null"/>
      <path class="p_0" stroke-width="1.5" stroke="#000000" id="svg_7" d="m200,57.68695c-34.85778,0 -62.90126,28.04348 -62.90126,62.90126l0,55.0386c0,5.76595 3.40715,13.62861 7.60057,17.55993l18.60829,17.29785c4.19342,3.93133 8.91101,11.26981 10.22145,16.24949s7.33848,9.1731 13.10443,9.1731l26.20886,0c5.76595,0 11.79399,-4.19342 13.10443,-9.1731s6.02804,-12.31816 10.22145,-16.24949l18.60829,-17.29785c4.19342,-3.93133 7.60057,-11.79399 7.60057,-17.55993l0,-55.0386c0.52418,-34.85778 -27.5193,-62.90126 -62.37708,-62.90126zm-30.14019,128.4234c-10.22145,0 -17.03576,-8.38683 -17.03576,-17.03576c0,-8.64892 8.12475,-14.41487 18.3462,-14.41487s18.3462,5.50386 18.3462,14.41487c0,8.64892 -9.69728,17.03576 -19.65664,17.03576zm30.14019,23.58797c-5.24177,0 -7.86266,-2.62089 -7.86266,-7.86266s5.24177,-15.72531 7.86266,-15.72531s7.86266,10.48354 7.86266,15.72531s-2.62089,7.86266 -7.86266,7.86266zm30.14019,-23.58797c-10.22145,0 -19.65664,-8.38683 -19.65664,-17.03576c0,-8.64892 8.12475,-14.41487 18.3462,-14.41487s18.3462,5.50386 18.3462,14.41487c0,8.64892 -6.8143,17.03576 -17.03576,17.03576z" fill="null"/>
        
      <path id="svg_12" class="path" d="m88.5,80.45313l272.5,79.54688l-231,189l-41.5,-268.54688z" stroke-opacity="null" stroke-width="1.5" stroke="#000000" fill="none"/>
      
          <path id="svg_13" class="path" d="m88.5,80.45313l272.5,79.54688l-231, 189l-41.5, -268.54688z" stroke-opacity="null" stroke-width=".3" stroke="#e29e29" fill="none" transform="rotate(45)"/>


      </g>
    
    </g>
        <animateMotion dur="5s" repeatCount="indefinite" rotate="auto-reverse">
          <mpath href="#svg_13"/>
        </animateMotion>
    </svg>
  </div>
<div class="canvasLluviaNeon" id="BodyCanvasLluvia">
    <canvas id="canvas-club"></canvas>
    <svg id="hs" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
    <defs>
        
        <filter id="glow">
              <fegaussianblur class="blur" result="coloredBlur" stddeviation="8"></fegaussianblur>
              <femerge>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="coloredBlur"></femergenode>
                <femergenode in="SourceGraphic"></femergenode>
              </femerge>
      </filter>
      <filter id="filter-broken" filterUnits="objectBoundingBox" x="0" y="0" width="100%" height="100%" color-interpolation-filters="sRGB">
          <feImage preserveAspectRatio="xMidYMid meet" xlink:href="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 800 800'%3E%3Crect width='70%25' height='70%25' fill='white'/%3E%3Ccircle cx='50%25' cy='50%25' r='500' %0Afill='none' stroke='black' stroke-width='950' stroke-dasharray='200'/%3E%3C/svg%3E" result="lense"/>
          <feDisplacementMap scale="10" xChannelSelector="B" yChannelSelector="G" in2="lense" in="SourceGraphic" result="glass-effect"/>
          <feMerge>
            <feMergeNode in="SourceGraphic"/>
            <feMergeNode in="glass-effect"/>
          </feMerge>
      </filter>
    </defs>

    <g>   
    <g fill="#1a1b1f" id="svg_8">
      <path class="p_0" stroke-width="1.5" stroke="#000000" id="svg_6" d="m302.47663,282.55895c-11.5319,-4.19342 -12.58025,5.76595 -33.28525,-1.57253l-40.62373,-14.67696l40.62373,-14.67696c20.705,-7.60057 21.75335,2.62089 33.28525,-1.57253c4.71759,-1.57253 7.60057,-7.86266 5.76595,-12.58025c-1.57253,-4.71759 -2.88297,-4.19342 -4.45551,-8.91101c-1.57253,-4.71759 -0.52418,-4.97968 -2.09671,-9.69728c-1.57253,-4.71759 -7.86266,-7.60057 -12.58025,-5.76595c-11.5319,4.19342 -5.76595,12.58025 -26.47095,20.18082l-62.63917,22.27753l-62.63917,-22.80171c-20.705,-7.60057 -15.20114,-15.9874 -26.47095,-20.18082c-4.71759,-1.57253 -11.00772,1.31044 -12.58025,5.76595c-1.57253,4.71759 -0.52418,4.97968 -2.09671,9.69728c-1.57253,4.71759 -2.88297,4.19342 -4.45551,8.91101c-1.57253,4.71759 1.31044,11.00772 5.76595,12.58025c11.5319,4.19342 12.58025,-5.76595 33.28525,1.57253l40.62373,14.67696l-40.62373,14.67696c-20.705,7.60057 -21.75335,-2.62089 -33.28525,1.57253c-4.71759,1.57253 -7.60057,7.86266 -5.76595,12.58025c1.57253,4.71759 2.88297,4.19342 4.45551,8.91101c1.57253,4.71759 0.52418,4.97968 2.09671,9.69728c1.57253,4.71759 7.86266,7.60057 12.58025,5.76595c11.5319,-4.19342 5.76595,-12.58025 26.47095,-20.18082l62.63917,-22.27753l62.63917,22.80171c20.705,7.60057 15.20114,15.9874 26.47095,20.18082c4.71759,1.57253 11.00772,-1.31044 12.58025,-5.76595c1.57253,-4.71759 0.52418,-4.97968 2.09671,-9.69728c1.57253,-4.71759 2.88297,-4.19342 4.45551,-8.91101c1.83462,-4.71759 -1.04835,-11.00772 -5.76595,-12.58025z" fill="null"/>
      <path class="p_0" stroke-width="1.5" stroke="#000000" id="svg_7" d="m200,57.68695c-34.85778,0 -62.90126,28.04348 -62.90126,62.90126l0,55.0386c0,5.76595 3.40715,13.62861 7.60057,17.55993l18.60829,17.29785c4.19342,3.93133 8.91101,11.26981 10.22145,16.24949s7.33848,9.1731 13.10443,9.1731l26.20886,0c5.76595,0 11.79399,-4.19342 13.10443,-9.1731s6.02804,-12.31816 10.22145,-16.24949l18.60829,-17.29785c4.19342,-3.93133 7.60057,-11.79399 7.60057,-17.55993l0,-55.0386c0.52418,-34.85778 -27.5193,-62.90126 -62.37708,-62.90126zm-30.14019,128.4234c-10.22145,0 -17.03576,-8.38683 -17.03576,-17.03576c0,-8.64892 8.12475,-14.41487 18.3462,-14.41487s18.3462,5.50386 18.3462,14.41487c0,8.64892 -9.69728,17.03576 -19.65664,17.03576zm30.14019,23.58797c-5.24177,0 -7.86266,-2.62089 -7.86266,-7.86266s5.24177,-15.72531 7.86266,-15.72531s7.86266,10.48354 7.86266,15.72531s-2.62089,7.86266 -7.86266,7.86266zm30.14019,-23.58797c-10.22145,0 -19.65664,-8.38683 -19.65664,-17.03576c0,-8.64892 8.12475,-14.41487 18.3462,-14.41487s18.3462,5.50386 18.3462,14.41487c0,8.64892 -6.8143,17.03576 -17.03576,17.03576z" fill="null"/>
        
      <path id="svg_12" class="path" d="m88.5,80.45313l272.5,79.54688l-231,189l-41.5,-268.54688z" stroke-opacity="null" stroke-width="1.5" stroke="#000000" fill="none"/>
      
          <path id="svg_13" class="path" d="m88.5,80.45313l272.5,79.54688l-231, 189l-41.5, -268.54688z" stroke-opacity="null" stroke-width=".3" stroke="#e29e29" fill="none" transform="rotate(45)"/>


      </g>
    
    </g>
        <animateMotion dur="5s" repeatCount="indefinite" rotate="auto-reverse">
          <mpath href="#svg_13"/>
        </animateMotion>
    </svg>
</div>
<div id="CosasDelDashBoard">
      <section>
        
        <div class="rad-body-wrapper rad-nav-min">
          <div class="container-fluid">
            <header class="rad-page-title">
              <small class="md-txt">Welcome Admin : {!! $nombre !!}</header>
            <div class="row">
              <div class="col-lg-3 col-xs-6">
              <img src="{{ asset('/img/icoDashboard/notDestroyed.jpg') }}" class="fa fa-vivas"></img>
                <div class="rad-info-box rad-txt-success">
                  <p class="heading elegantshadow">Comunidades Vivas</p>
                  <span class="value"><span>{!! $comunidadesVivas !!}</span></span>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
              <img src="{{ asset('/img/icoDashboard/destroyed.jpg') }}" class="fa fa-vivas"></img>
                <div class="rad-info-box rad-txt-primary">
                  <p class="heading elegantshadow">Comunidades Destruidas</p>
                  <span class="value"><span>{!! $comunidadesDestruidas !!}</span></span>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
              <img src="{{ asset('/img/icoDashboard/male1.png') }}" class="fa fa-vivas"></img>
                <div class="rad-info-box rad-txt-danger">
                  <p class="heading elegantshadow">Personas vivas</p>
                  <span class="value"><span>{!! $personasVivas !!}</span></span>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
              <img src="{{ asset('/img/icoDashboard/death.png') }}" class="fa fa-vivas"></img>
                <div class="rad-info-box">
                  <p class="heading elegantshadow">Personas muertas</p>
                  <span class="value"><span>{!! $personasMuertas !!}</span></span>
                </div>
              </div>
            </div>
            <div class="row">                        <div class="card-body table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <th>ID</th>
                                    <th>Nomkbre</th>
                                    <th>Email</th>
                                    <th>Balance</th>
                                    <th>Rol</th>
                                    <th>Comunitat</th>
                                </thead>
                                <tbody>
                                @foreach($personas as $p)
                                <tr>
                                        <td>{{ $p->id }}</td>
                                        <td>{{ $p->name }}</td>
                                        <td>{{ $p->email }}</td>
                                        <td>{{ $p->balance }}</td>
                                        <td>{{ $p->rol_id }}</td>
                                        <td>{{ $p->comunitat_id }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
              <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">People</h3>
                  </div>
                  <div class="panel-body">
                  <div id="pie-chart" ></div>
                  </div>

                </div>
              </div>
              <div class="col-lg-3 col-md-4 col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Activity</h3>
                  </div>
                  <div class="panel-body">
                    <div class="rad-activity-body">
                    <div class="rad-list-group group">
                      <div class="rad-list-group-item">
                                            <a onclick="myDisaster()"  class="button1">Disaster</a>	</div>
                      </div>
                      <div class="rad-list-group-item">
                                            <a onclick="myDisaster2()"  class="button1">Disaster</a>	</div>
                      </div>

                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        
      </section>
    </div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js'></script>
<script src="./js/anime.min.js"></script>
<script src="./js/script.js"></script>
<script src="./js/scriptLLuviaNEON.js"></script>
<script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>
<script type="text/javascript">
  Morris.Donut({
  element: 'pie-chart',
  data: [
    {label: "Alive", value: {!! $personasVivas !!}},
    {label: "Death", value: {!! $personasMuertas !!}},
    {label: "Unknown", value: {!! $personasNoSeSabe !!}},
  ]
});
  function myDisaster() {

    
        var y = document.getElementById("BodyEnteroCosas");
        var z = document.getElementById("CosasDelDashBoard");
        y.style.display = "block";
        z.style.display = "none";
        var desastres = ["Tsunami Devastador", "Lluvias Torrenciales", "Tormenta Electrica",'Vientos Huracanados'];

        var x = document.getElementById("loading");
        x.textContent=desastres[Math.floor(Math.random() * desastres.length)];
        alphabet = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    letter_count = 0;
    el = $("#loading");
    word = el.html().trim();
    finished = false;

    el.html("");
    for (var i = 0; i < word.length; i++) {
        el.append("<span>" + word.charAt(i) + "</span>");
    }

    setTimeout(write, 75);
    incrementer = setTimeout(inc, 1000);

    function write() {
        for (var i = letter_count; i < word.length; i++) {
            var c = Math.floor(Math.random() * 36);
            $("span")[i].innerHTML = alphabet[c];
        }
        if (!finished) {
            setTimeout(write, 75);
        }
    }

    function inc() {
        $("span")[letter_count].innerHTML = word[letter_count];
        $("span:eq(" + letter_count + ")").addClass("glow");
        letter_count++;
        if (letter_count >= word.length) {
            finished = true;
        } else {
            setTimeout(inc, 1000);
        }
    }
    setTimeout(function () {
            if (y.style.display = "block") {
                y.classList.add("animate__animated");
                y.classList.add("animate__fadeOut");
                y.classList.add("animate__fast");
                setTimeout(function() {
                    y.classList.remove("animate__animated")
                    y.classList.remove("animate__fadeOut")
                    y.classList.remove("animate__fast")
                    y.style.display = "none";
                    z.style.display = "block";
                    z.classList.remove("animate__animated")
                    z.classList.remove("animate__fadeIn")
                    z.classList.remove("animate__fast")
                    window.location.href = "{{ route('Matar')}}";
                }, 5000);
            }
        }, 20000);
    }
    function myDisaster2() {
        var y = document.getElementById("BodyCanvasLluvia");
        var z = document.getElementById("CosasDelDashBoard");
        y.style.display = "block";
        z.style.display = "none";
        var desastres = ["Tsunami", "Lluvia", "Maricon"];

        var x = document.getElementById("loading");
        x.textContent=desastres[Math.floor(Math.random() * desastres.length)];
        alphabet = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    letter_count = 0;
    el = $("#loading");
    word = el.html().trim();
    finished = false;

    el.html("");
    for (var i = 0; i < word.length; i++) {
        el.append("<span>" + word.charAt(i) + "</span>");
    }

    setTimeout(write, 75);
    incrementer = setTimeout(inc, 1000);

    function write() {
        for (var i = letter_count; i < word.length; i++) {
            var c = Math.floor(Math.random() * 36);
            $("span")[i].innerHTML = alphabet[c];
        }
        if (!finished) {
            setTimeout(write, 75);
        }
    }

    function inc() {
        $("span")[letter_count].innerHTML = word[letter_count];
        $("span:eq(" + letter_count + ")").addClass("glow");
        letter_count++;
        if (letter_count >= word.length) {
            finished = true;
        } else {
            setTimeout(inc, 1000);
        }
    }
    setTimeout(function () {
            if (y.style.display = "block") {
                y.classList.add("animate__animated");
                y.classList.add("animate__fadeOut");
                y.classList.add("animate__fast");
                setTimeout(function() {
                    y.classList.remove("animate__animated")
                    y.classList.remove("animate__fadeOut")
                    y.classList.remove("animate__fast")
                    y.style.display = "none";
                    z.style.display = "block";
                    z.classList.remove("animate__animated")
                    z.classList.remove("animate__fadeIn")
                    z.classList.remove("animate__fast")
                }, 5000);
            }
        }, 10000);
    }



</script>
</html>