<!DOCTYPE html>
<html lang="es">
<head>
    <title>New Order</title>
    <link href="{{ asset('/css/bootstrap1.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>

    <meta charset="UTF-8">
    <meta name="description" content="Interfaz">
    <meta name="keywords" content="New Order">
    <meta name="author" content="Shahraz,Toni,Rafa">

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/css3.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bungee&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body id="main">

    <div class="container">
    <!--<nav class="barraNav">-->
        <nav class="page__menu page__custom-settings menu">
            <ul class="menu__list r-list">
              <li class="menu__group"><a href="{{ asset('/pagina2') }}" class="menu__link r-link text-underlined">Tienda </a></li>
              <li class="menu__group"><a href="#0" class="menu__link r-link text-underlined">Pantalla4</a></li>
              <li class="menu__group"><a href="#0" class="menu__link r-link text-underlined">Pantalla5 </a></li>
            </ul>
          </nav>
    <div class="sidebar">
    <a onclick="armasShop()">Armas</a>
    <a onclick="ChalecosShop()">Chalecos</a>
    <a  onclick="NavajasShop()">Navajas</a>
    <a onclick="defensaShop()">Defensa Personal</a>
    </div>
    <div class="grid-container">
        <div class="Empresa1">
            <div class="Header">
                <img src="../img/pepsi.png">
                <h2>Pepsi S.L.</h2>
                <h3>Manipulador de pedidos</h3>
                <h4>Torrejon de Ardoz</h4>
                <p>Somos Abanti Empleo, una consultoría de selección y trabajo temporal con cobertura en todo el territorio nacional. Con experiencia en las áreas de selección, formación y contratación, nacemos con la misión de unificar las necesidades de nuestros clientes en el ámbito de los RR.HH., de manera que podamos ser tu partner de confianza, aportando soluciones ante cualquier necesidad de servicios en materia de personal de tu empresa.
                </p>
                <a href="something" class="button1">Trabajar</a>

            </div>
        </div>
        <div class="Empresa2">
            <div class="Header">
                <img src="../img/pepsi.png">
                <h2>Pepsi S.L.</h2>

                <h3>Manipulador de pedidos</h3>
                <h4>Torrejon de Ardoz</h4>
                <p>Somos Abanti Empleo, una consultoría de selección y trabajo temporal con cobertura en todo el territorio nacional. Con experiencia en las áreas de selección, formación y contratación, nacemos con la misión de unificar las necesidades de nuestros clientes en el ámbito de los RR.HH., de manera que podamos ser tu partner de confianza, aportando soluciones ante cualquier necesidad de servicios en materia de personal de tu empresa.
                </p>                <a href="something" class="button1">Trabajar</a>

            </div>
        </div>
        <div class="Empresa3">
            <div class="Header">
                <img src="../img/pepsi.png">
                <h2>Pepsi S.L.</h2>

                <h3>Manipulador de pedidos</h3>
                <h4>Torrejon de Ardoz</h4>
                <p>Somos Abanti Empleo, una consultoría de selección y trabajo temporal con cobertura en todo el territorio nacional. Con experiencia en las áreas de selección, formación y contratación, nacemos con la misión de unificar las necesidades de nuestros clientes en el ámbito de los RR.HH., de manera que podamos ser tu partner de confianza, aportando soluciones ante cualquier necesidad de servicios en materia de personal de tu empresa.
                </p>                <a href="something" class="button1">Trabajar</a>

            </div>
        </div>
      </div>

    <footer>
    <ul id="webticker" >
            <li data-update="item1">barrio Matalascañá: esta madrugada se ha producido una explosion bioQuimica en las inmediaciones del centro de recursos organicos del barrio Matalascañá. de las 10 victimas del atentado solo una ha sido de caracter mortal pero toda la cosecha de esta temporada se ha visto afectada...
.</li>
            <li data-update="item2">Distrito Kingujo: las victimas del maremoto del distrito Kingujo siguen en aumento. 1438 victimas mortales, 4873 victimas graves y 7392 leves...
</li>
</ul>
        <h2>&copy;Copyright per Shahraz,Toni i Rafa<h2>
    </footer>
</body>

</html>


<script type="text/javascript">
AOS.init();
$("#webticker").webTicker({
    height:'75px',
    duplicate:true,
    rssfrequency:0,
    startEmpty:false,
    hoverpause:false,
    transition: "ease"
});
function armasShop(){

    document.getElementById("armas").style.display = "grid";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "none";

}
function ChalecosShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "grid";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "none";
}
function NavajasShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "block";
    document.getElementById("DefensaPersonal").style.display = "none";

}
function defensaShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "block";

}
</script>
