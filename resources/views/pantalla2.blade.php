<!DOCTYPE html>
<html lang="es">
<head>
    <title>New Order</title>
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/nav2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/ani.css') }}">
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>

    <meta charset="UTF-8">
    <meta name="description" content="Interfaz">
    <meta name="keywords" content="New Order">
    <meta name="author" content="Shahraz,Toni,Rafa">

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/css2.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bungee&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body id="main">
    <div class="page">
        <header tabindex="0">
                <div class="containerNav">
                        <ul class="menu__list r-list menu  page__custom-settings">
                            <div class="logo">
                                <p>N|E</p>
                            </div>
                            <li class="menu__group"><a href="{{ asset('/') }}" class="menu__link r-link text-underlined">Inici </a></li><li>
                            <li class="menu__group"><a href="{{ asset('/pagina2') }}" class="menu__link r-link text-underlined">Tienda </a></li>
                            <li class="menu__group"><a href="{{ asset('/pagina4') }}" class="menu__link r-link text-underlined">Parametrizable</a></li>                            
                            <li>
                        </ul>
                </div>
            </header>
            <div id="nav-container">
            <div class="button" tabindex="0">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
            <div id="nav-content" tabindex="0">
                <ul>
                    <li><a onclick="armasShop()" href="#0">Armas</a></li>
                    <li><a onclick="ChalecosShop()" href="#0">Chalecos</a></li>
                    <li><a onclick="NavajasShop()" href="#0">Navajas</a></li>
                    <li><a onclick="defensaShop()" href="#0">Defensa Personal</a></li>
                    <li><a onclick="trabajoShop()" href="#0">Trabajo</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="marcador">
            <label>{!! $Objecte->balance !!}   <img src="{{ asset('/img/billeteSIENIcon.png') }}" alt=""></label>
        </div>
    <div class="row" id="armas">
                    <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInUp" id="element1">
                        <div class="thumbnail product-box">
                            <img src="{{ asset('/img/ak47.jpg') }}" alt="">
                            <div class="caption">
                                <h3><a href="#">AK 47</a></h3>
                                <p >Price: <strong>1,000 €</strong>  </p>
                                <p><a href="#" style="color:teal">Tipo: Fusil de Asalto</a></p>
                                <p>El Avtomat Kalashnikova modelo 1947, de calibre 7,62 mm</p>
                                <p><a href="/Controlador/buyItem/1" class="button1">Comprar</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInUp"  id="element2">
                        <div class="thumbnail product-box">
                            <img src="{{ asset('/img/glock.jpg') }}" alt="">
                            <div class="caption">
                                <h3><a href="#">Glock 19 </a></h3>
                                <p>Price: <strong>100 €</strong>  </p>
                                <p><a href="#" style="color:teal">Tipo: Pistola semiautomática</a></p>
                                <p>La Glock modelo 19 es una pistola semiautomatica, de calibre 9mm </p>
                                <p><a href="/Controlador/buyItem/2" class="button1">Comprar</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInUp" id="element3">
                        <div class="thumbnail product-box">
                            <img src="{{ asset('/img/recortada.jpg') }}" alt="">
                            <div class="caption">
                                <h3><a href="#">SPAS 12 </a></h3>
                                <p>Price: <strong>500 €</strong>  </p>
                                <p><a href="#" style="color:teal">Tipo: Escopeta </a></p>
                                <p>La SPAS-12 es una escopeta de calibre 18,53 mm</p>
                                <p><a href="/Controlador/buyItem/3" class="button1">Comprar</a></p>
                            </div>
                        </div>
                    </div>
    </div>
        <div class="row" id="Chalecos">
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInDown">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/chalecoMierda.png') }}" alt="">
                    <div class="caption">
                        <h3><a href="#">Chaleco Basico</a></h3>
                        <p>Price : <strong>30 €</strong>  </p>
                        <p><a href="#" style="color:teal">Tipo: I</a></p>
                        <p>Es una prenda protectora que absorbe el impacto de balas 22LRHV</p>
                        <p><a href="/Controlador/buyItem/4" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInDown">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/chalecoavg.png') }}" alt="">
                    <div class="caption">
                        <h3><a href="#">Chaleco Estandar</a></h3>
                        <p>Price : <strong>50 €</strong>  </p>
                        <p><a href="#" style="color:teal">Tipo: II</a></p>
                        <p>Es una prenda protectora que absorbe el impacto de balas 9mm</p>
                        <p><a href="/Controlador/buyItem/5" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInDown">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/chalecoBueno.png') }}" alt="">
                    <div class="caption">
                        <h3><a href="#"> Chaleco Militar</a></h3>
                        <p>Price : <strong>100 €</strong></p>
                        <p><a href="#" style="color:teal">Tipo IIIA</a></p>
                        <p>Es una prenda protectora que absorbe el impacto de balas 7.62mm</p>
                        <p><a href="/Controlador/buyItem/6" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="Navajas">
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInLeft">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/navajaderescate.jpg') }}" alt="">
                    <div class="caption">
                        <h3><a href="#">Navaja</a></h3>
                        <p>Price : <strong>20 €</strong>  </p>
                        <p><a href="#" style="color:teal">Tipo: Damasco</a></p>
                        <p>Navaja basica para autodefensa o otras necesidades diarias</p>
                        <p><a href="/Controlador/buyItem/7" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInLeft">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/machete.jpg') }}" alt="">
                    <div class="caption">
                        <h3><a href="#">Machete</a></h3>
                        <p>Price : <strong>30 €</strong>  </p>
                        <p><a href="#" style="color:teal">Tipo: Machete </a></p>
                        <p>Machete , mayor tamaño y corte para las necesidades que le surgan</p>
                        <p><a href="/Controlador/buyItem/8" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInLeft">
                <div class="thumbnail product-box">
                    <img src="{{ asset('/img/bigrambo.jpg') }}" alt="">
                    <div class="caption">
                        <h3><a href="#">BIG RAMBO III</a></h3>
                        <p>Price : <strong>50 €</strong>  </p>
                        <p><a href="#" style="color:teal">Tipo: Machete</a></p>
                        <p>Cuchillo con hoja de acero , absorbe las vibraciones durante el uso</p>
                        <p><a href="/Controlador/buyItem/9" class="button1">Comprar</a></p>
                    </div>
                </div>
            </div>
        </div>

            <div class="row" id="DefensaPersonal">
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/kitsupervivencia.png') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Kit Supervivencia</a></h3>
                            <p>Price : <strong>200 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: Kit Supervivencia</a></p>
                            <p>Kit de Supervivencia Militar tiene: Navaja, brujula , linterna , etc  </p>
                            <p><a href="/Controlador/buyItem/10" class="button1">Comprar</a></p>
                        </div>
                    </div> 
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/armar-un-botiquin-de-primeros-auxilios-1-compressor.jpg') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Kit Primeros Auxilios</a></h3>
                            <p>Price : <strong>100 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: Kit Medico</a></p>
                            <p>Es un maletin que contiene medicamentos y herramientas</p>
                            <p><a href="/Controlador/buyItem/11" class="button1">Comprar</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/spray.png') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Spray Pimienta</a></h3>
                            <p>Price : <strong>10 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: Defensa Personal</a></p>
                            <p>Articulo de defensa personal que tira gas pimienta</p>
                            <p><a href="/Controlador/buyItem/12" class="button1">Comprar</a></p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row" id="Trabajo">
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/militar.jpg') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Militar</a></h3>
                            <p>Trabajar por  : <strong>200 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: De Riesgo</a></p>
                            <p>Salva a gente importante por una gran suma de dinero  </p>
                            <p><a href="/Controlador/trabajar/200" class="button1">Aceptar Trabajo</a></p>
                        </div>
                    </div> 
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/sicario.png') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Sicario</a></h3>
                            <p>Trabajar por  : <strong>100 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: De Riesgo</a></p>
                            <p>Mata gente importante por una gran suma de dinero</p>
                            <p><a href="/Controlador/trabajar/100" class="button1">Aceptar Trabajo</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6 animate__animated animate__bounceInRight">
                    <div class="thumbnail product-box">
                        <img src="{{ asset('/img/nuclear.png') }}" alt="">
                        <div class="caption">
                            <h3><a href="#">Basusero Espacial</a></h3>
                            <p>Trabajar por : <strong>10 €</strong>  </p>
                            <p><a href="#" style="color:teal">Tipo: De Riesgo</a></p>
                            <p>Limpia cosas espaciales por una pequeña suma de dinero</p>
                            <p><a href="/Controlador/trabajar/10" class="button1">Aceptar Trabajo</a></p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    <a class="me" href="https://codepen.io/uzcho_/pens/popular/?grid_type=list" target="_blank"></a>
    <footer>
    <ul id="webticker" >
            <li data-update="item1">barrio Matalascañá: esta madrugada se ha producido una explosion bioQuimica en las inmediaciones del centro de recursos organicos del barrio Matalascañá. de las 10 victimas del atentado solo una ha sido de caracter mortal pero toda la cosecha de esta temporada se ha visto afectada...
.</li>
            <li data-update="item2">Distrito Kingujo: las victimas del maremoto del distrito Kingujo siguen en aumento. 1438 victimas mortales, 4873 victimas graves y 7392 leves...
</li>
</ul>
        <h2>&copy;Copyright per Shahraz,Toni i Rafa<h2>
    </footer>
</body>

</html>


<script type="text/javascript">
AOS.init();
$("#webticker").webTicker({
    height:'75px',
    duplicate:true,
    rssfrequency:0,
    startEmpty:false,
    hoverpause:false,
    transition: "ease"
});
function armasShop(){

    document.getElementById("armas").style.display = "block";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "none";
    document.getElementById("Trabajo").style.display = "none";
}
function ChalecosShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "block";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "none";    
    document.getElementById("Trabajo").style.display = "none";
}
function NavajasShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "block";
    document.getElementById("DefensaPersonal").style.display = "none";
    document.getElementById("Trabajo").style.display = "none";
}
function defensaShop(){

    document.getElementById("armas").style.display = "none";
    document.getElementById("Chalecos").style.display = "none";
    document.getElementById("Navajas").style.display = "none";
    document.getElementById("DefensaPersonal").style.display = "block";
    document.getElementById("Trabajo").style.display = "none";
}

function trabajoShop(){

document.getElementById("armas").style.display = "none";
document.getElementById("Chalecos").style.display = "none";
document.getElementById("Navajas").style.display = "none";
document.getElementById("DefensaPersonal").style.display = "none";
document.getElementById("Trabajo").style.display = "block";
}
</script>
