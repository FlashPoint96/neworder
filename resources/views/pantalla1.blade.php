<!DOCTYPE html>
<html lang="es">
<head>
    <title>New Order</title>
    <script src="{{ mix('js/app.js') }}"></script>
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/input.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>
    <meta charset="UTF-8">
    <meta name="description" content="Interfaz">
    <meta name="keywords" content="New Order">
    <meta name="author" content="Shahraz,Toni,Rafa">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/maincss.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bungee&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>


<body id="main">
    <header></header>
    <div class="container">
        <nav class="page__menu page__custom-settings menu">
            <ul class="menu__list r-list">
                <div class="logo">
                    <p>N|E</p>
                </div>
                <li class="menu__group"><a href="#Home" class="menu__link r-link text-underlined inicinav">Inici</a></li>
                <li class="menu__group"><a href="#About" class="menu__link r-link text-underlined">About</a></li>
                <li class="menu__group"><a href="#Login" class="menu__link r-link text-underlined">Login </a></li>
            </ul>
        </nav>
    <section id="Home">
        <h1 id="NewOrder" class="animate__animated animate__zoomInDown animate__slow">New Order</h1>
        <p id="autores">Per: Shahraz Ahmad , Toni Lupiañez y Rafa Gonzalez</p>
    </section>
    <section id="About" >
        <h1 id="New Order" data-aos="fade-right"
    data-aos-offset="300"
    data-aos-easing="ease-in-sine">About</h1>

        <p data-aos="fade-right"
    data-aos-offset="300"
    data-aos-easing="ease-in-sine">Benvinguts a New Order Portal per Pura Calle.
            Durant el periode de 2049-2055 ha esdevingut una brutal sacsejada social.Ara amb l'arribada de una nova ordre mundial els integrants de Pura Calle em creat aquesta web per facilitar les transaccions i la comunicacio al món. </p>
    </section>
    <section id="Login">
    <div id="login-box"  data-aos="fade-left"
    data-aos-offset="500"
    data-aos-easing="ease-in-sine">
        <form name="Login" method="POST" action="/registeroLogin" onsubmit="return pruebaPassword()" id="formulario">
        {{ csrf_field() }}
            <div id="loginForm">
                <h1 id="login">Login</h1>
                <div class="group">
                    <input type="text" name="nombre1" >
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Name</label>
                </div>
                <br>
                <div class="group">
                    <input type="password" name="contrasenaaa" >
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Password</label>
                </div>
                <br>
                <input type="submit" value="Envia" name="Enviar" id="enviar"/>
                <button type="button" onclick="registerFormulario()" id="registro">Registro</button>
            </div>
            <div id="RegisterForm">
                <button type="button" onclick="loginFormulario()" id="paraAtras"><</button>
                <h1 id="Register">Registro</h1>
                <!--<label for="nom">
                <input type="text" name="nom" placeholder="Nom" class="txtInputs">
                </label>-->
                <div class="group">
                    <input type="text" name="nombre" >
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Name</label>
                </div>
                <br>
                <div class="group">
                    <input type="password" name="contrasena" id="Pass">
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="Contrasena" id="contrasena">Password</label>
                </div>
                <!--<label for="Contrasena" id="contrasena">
                <input type="password" name="contrasena" placeholder="Password" class="txtInputs" id="Pass">
                </label>-->
                <br>
                <div class="group" >
                    <input type="password" name="contrasenaa"  id="confirmPass">
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="Contrasena" id="Confcontrasena" >Confirm Password</label>
                </div>
                <!--<label for="Contrasena" id="Confcontrasena">
                <input type="password" name="contrasena1" placeholder="Confirmar Password" class="txtInputs" id="confirmPass" >
                </label>-->
                <br>
                <input type="submit" value="Registrar" name="Enviar" id="registrarInput" />
            </div>
        </form>
    </div>
    </section>
    <footer>
        <h2>&copy;Copyright per Shahraz,Toni i Rafa<h2>

    </footer>
</div>

</body>

</html>

@section('page-js-script')
<script type="text/javascript">
    AOS.init();

function loginFormulario(){

document.getElementById("loginForm").style.display = "block";
document.getElementById("RegisterForm").style.display = "none";
document.getElementById("formulario").style.height = "450px";
}
function registerFormulario(){

    document.getElementById("loginForm").style.display = "none";
    document.getElementById("RegisterForm").style.display = "block";
    document.getElementById("formulario").style.height = "550px";
}
            function pruebaPassword() {
                var pass = document.getElementById("Pass")
                    , pass1 = document.getElementById("confirmPass");
                if (pass.value != pass1.value) {
                    swal("Las contraseñas no son identicas");
		return false;
                } else {
		return true;
                }
            }

</script>

