<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objetos extends Model
{
    protected $table = 'Objetos';
    protected $primaryKey = 'id';
    protected $fillable = ['Objetos','idObjeto'];

    public function user()
    {
        return $this->belongsTo(user::class);
    }

}
