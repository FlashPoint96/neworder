<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Models\Comunitat;
use App\Models\User;
use App\Models\Objetos;
use Illuminate\Support\Facades\Hash;
use App\Models\Rol;
use Auth;


class Controlador extends Controller
{
    public function RegisterLogin(Request $request)
    {
        $loQueHaEnviado = $request->Enviar;
        if($loQueHaEnviado == "Registrar"){
            $comunidad = Comunitat::all()->random();
            $numero = $comunidad->id;
            $usuario =  User::create([
                'name' => $request['nombre'],
                'email' => $request['nombre']."@gmail.com",
                'password' => Hash::make($request['contrasena']),
                'balance' =>'100',
            ]);
            $usuario -> comunitat_id = $numero;
            $usuario -> rol_id = 2;
            $usuario->save();

            return view("".'pantalla1Logeado'); 
        }else if($loQueHaEnviado == "Envia"){
            $nombre = $request->nombre1;
            $contrase = $request->contrasenaaa;
            if (Auth::attempt(['name' => $nombre, 'password' => $contrase])) {
                $objeto = Auth::user();
                $rol = Rol::find($objeto->rol_id);
                if($rol->EnumRol == "UsuariAdministradorNewOrder"){
                    $roles = Rol::all();
                    $objeto = Auth::user()->name;
            $comunidades = Comunitat::all();
            $comunidadesDestruidas = 0;
            $comunidadesVivas = 0;

            $personas = User::all();
            $personasMuertas = 0;
            $personasVivas = 0;
            $personasNoSeSabe = 0;
            foreach ($comunidades as $c){ 
                if($c->Activa == 0){
                    $comunidadesVivas++;
                }else{
                    $comunidadesDestruidas++;
                }
            }
            foreach ($personas as $p){ 
                if($p->balance <= 0){
                    $personasMuertas++;
                }else if($p->balance>0){
                    $personasVivas++;
                }else{
                    $personasNoSeSabe++;
                }
            }
           
            return view('pantalla5',[
                'nombre'  => $objeto,
                'comunidadesDestruidas'   => $comunidadesDestruidas,
                'comunidadesVivas' => $comunidadesVivas,
                'personasVivas'   => $personasVivas,
                'personasMuertas' => $personasMuertas,
                'personasNoSeSabe' =>$personasNoSeSabe,
                'personas' => $personas
            ]);
                }else{
                    return view('pantalla2')->with('Objecte', $objeto);
                }
            }else{
                alert()->message('Notificación solo con mensaje');
            }
        }
        echo $loQueHaEnviado;
    }
    public function logout(){
        if(Auth::check()){
            Auth::logout();
            return view("".'pantalla1');
        }
    }
    public function pagina4()
    {
    if(Auth::check()){
        $objeto = Auth::user()->name;
        return view('pantalla4')->with('Objecte', $objeto);
    }else{
        return view("".'pantalla1');
    }
    }

    public function pagina5()
    {
        if(Auth::check()){              
            $objeto = Auth::user()->name;
            $comunidades = Comunitat::all();
            $comunidadesDestruidas = 0;
            $comunidadesVivas = 0;

            $personas = User::all();
            $personasMuertas = 0;
            $personasVivas = 0;
            $personasNoSeSabe = 0;
            foreach ($comunidades as $c){ 
                if($c->Activa == 0){
                    $comunidadesVivas++;
                }else{
                    $comunidadesDestruidas++;
                }
            }
            foreach ($personas as $p){ 
                if($p->balance <= 0){
                    $personasMuertas++;
                }else if($p->balance>0){
                    $personasVivas++;
                }else{
                    $personasNoSeSabe++;
                }
            }
           
            return view('pantalla5',[
                'nombre'  => $objeto,
                'comunidadesDestruidas'   => $comunidadesDestruidas,
                'comunidadesVivas' => $comunidadesVivas,
                'personasVivas'   => $personasVivas,
                'personasMuertas' => $personasMuertas,
                'personasNoSeSabe' =>$personasNoSeSabe,
                'personas' => $personas
            ]);
        }else{
            return view("".'pantalla1');
        }
    }

    public function pagina3()
    {   
        if(Auth::check()){
            return view("".'pantalla1Logeado'); 
        }else{
            return view("".'pantalla1'); 
        }
    }
    public function pagina2()
    {   
        if(Auth::check()){        
            $objeto = Auth::user();
        return view('pantalla2')->with('Objecte', $objeto);
        }else{
            return view("".'pantalla1'); 
        }
    }

    public function LogeadoNo()
    {  

        if(Auth::check()){
            $objeto = Auth::user();
            $rol = Rol::find($objeto->rol_id);
            if($rol->EnumRol == "UsuariAdministradorNewOrder"){
                $objeto = Auth::user()->name;
                $comunidades = Comunitat::all();
                $comunidadesDestruidas = 0;
                $comunidadesVivas = 0;
    
                $personas = User::all();
                $personasMuertas = 0;
                $personasVivas = 0;
                $personasNoSeSabe = 0;
                foreach ($comunidades as $c){ 
                    if($c->Activa == 0){
                        $comunidadesVivas++;
                    }else{
                        $comunidadesDestruidas++;
                    }
                }
                foreach ($personas as $p){ 
                    if($p->balance <= 0){
                        $personasMuertas++;
                    }else if($p->balance>0){
                        $personasVivas++;
                    }else{
                        $personasNoSeSabe++;
                    }
                }
               
                return view('pantalla5',[
                    'nombre'  => $objeto,
                    'comunidadesDestruidas'   => $comunidadesDestruidas,
                    'comunidadesVivas' => $comunidadesVivas,
                    'personasVivas'   => $personasVivas,
                    'personasMuertas' => $personasMuertas,
                    'personasNoSeSabe' =>$personasNoSeSabe,
                    'personas' => $personas           
                ]);              
            }else{
                return view("".'pantalla1Logeado'); 
            }
        }else{
            return view("".'pantalla1'); 
        }
    }
    public function agregarItem(Request $request) {

        $objecto = new Objetos();
        /*Dentro de la tabla objeto, en la columna user_id, guardame la id de la sesion activa(la id de quein se acaba 
        de hacer login)*/
        /*De la tabla objeto, en la columna ObjectName, guardame el valor que el usuario(al crear el objeto en el html)
        ha puesto en el campo html llamado nom*/
        $objecto->NomObjeto = $request->nom;
        $objecto->DescripcioObjeto = $request->descripcion;
        $objecto->PreuObjeto =$request->LvLUse;
        $objecto->Categoria =$request->enumCategoria;
        $path = $request->file('Foto')->store('Fotos');
        $request->file('Foto')->move(public_path('Fotos'), $path);
        $objecto->Path = $path;
        $objecto->save();
        return $objecto;
    }
    public function comprarObjeto($id) {
        $objeto = Objetos::find($id);
        $user = Auth::user();
        if($user->balance >= $objeto->PreuObjeto){
            //lo compra
            $objetosComprador = array();
            if($user->Objetos != null){
                $objetosComprador = $user->Objetos;
            }
            array_push($objetosComprador, $objeto);
            $user->balance -=$objeto->PreuObjeto;
            $user->Objetos = $objetosComprador;
            $user->save();
            return view('pantalla2')->with('Objecte', $user);
        }
    }

    public function trabajarDinero($id) {
        $user = Auth::user();
        $user->balance +=$id;
        $user->save();
        return view('pantalla2')->with('Objecte', $user);
    }

    public function MatarCiudad()
    {
        $comunidad = Comunitat::all()->random();
        $comunidad->Activa = 1;
        $comunidad->save();
        $users = User::all();
        foreach ($users as $u){ 
            if($u->comunitat_id == $comunidad->id){
                $u->Balance = -100;
                $u->save();
            }
        }
        $objeto = Auth::user()->name;
        $comunidades = Comunitat::all();
        $comunidadesDestruidas = 0;
        $comunidadesVivas = 0;

        $personas = User::all();
        $personasMuertas = 0;
        $personasVivas = 0;
        $personasNoSeSabe = 0;
        foreach ($comunidades as $c){ 
            if($c->Activa == 0){
                $comunidadesVivas++;
            }else{
                $comunidadesDestruidas++;
            }
        }
        foreach ($personas as $p){ 
            if($p->balance <= 0){
                $personasMuertas++;
            }else if($p->balance>0){
                $personasVivas++;
            }else{
                $personasNoSeSabe++;
            }
        }
       
        return view('pantalla5',[
            'nombre'  => $objeto,
            'comunidadesDestruidas'   => $comunidadesDestruidas,
            'comunidadesVivas' => $comunidadesVivas,
            'personasVivas'   => $personasVivas,
            'personasMuertas' => $personasMuertas,
            'personasNoSeSabe' =>$personasNoSeSabe,
            'personas' => $personas           
        ]);
    }

    public function showObject() {
        $objeto = Objetos::all();
        return view('pantalla2')->with('Objecte', $objeto);
        
       // $objeto = User::with('objetos')->find(Auth::id())->objetos;
       // return view('objeto')->with('Objecte', $objeto);
    }


}
